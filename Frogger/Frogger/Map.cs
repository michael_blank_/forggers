﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frogger
{
    class Map
    {
        public Tile[,] tileList = new Tile[16,9];
        public Vector2 tileSize;
        public Map(int windowSize, int windowWidth, int level)
        {
            tileSize = new Vector2(windowSize / 16, windowWidth / 9);
            for (int i = 0; i < 16; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                   tileList[i, j] = new Tile(tileSize.X * i, tileSize * j, new Vector2(tileSize.X/256, tileSize.Y/256), 0);
                }
            }
        }

        public void update()
        {

        }

        public void draw(GameTime gameTime, SpriteBatch sB)
        {
            for (int i = 0; i < 16; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    tileList[i, j].draw(gameTime, sB);
                }
            }
        }
    }
}
