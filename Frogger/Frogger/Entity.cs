﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frogger
{
    class Entity
    {
        Vector2 position;
        private float v;
        Vector2 size;

        public Entity(float x, float y, float v, int width, int type)
        {
            this.size = new Vector2(x, y);
            this.v = v;
            this.position = new Vector2(x, y);           
        }

        public void update()
        {
            size.X = size.X + v;
        }

        public void draw()
        {

        }

       
    }
}
